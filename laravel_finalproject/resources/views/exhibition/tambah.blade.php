@extends('layout.master')
@section('title')
   Halaman Tambah Exhibition
@endsection

@section('content')
<form method='post' action="/exhibition"  enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Exhibition</label>
      <input type="text" name='judul' class="form-control" >
    </div>

    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Gambar Poster</label>
        <input type="file" name='gambar' class="form-control-file" >
      </div>

      @error('gambar')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror


    <div class="form-group">
    <label>Category</label>
   <select name="category_id_category" class="form-control">

        <option value="">--pilih category--</option>
        @forelse ($category as $item)
            <option value="{{$item->id_category}}">{{$item->judul_category}} </option>
        @empty
            <option value="">Belum ada data</option>
        @endforelse


   </select>
    </div>
  
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label >Deskripsi</label>
      <textarea class="form-control" name='deskripsi' cols='30' rows='10'> </textarea>
    </div>
    
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    
   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection