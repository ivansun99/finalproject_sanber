@extends('layout.master')
@section('title')
    Halaman Edit Exhibition
@endsection


@section('content')
<form method='post' action="/exhibition/{{$exhibition->id_exhibition}}"  enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Judul Exhibition</label>
      <input type="text" name='judul' value="{{$exhibition->judul}} " class="form-control" >
    </div>

    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Gambar Poster</label>
        <img src="{{asset('/image/'. $exhibition->gambar)}} " alt="gambar" class="img-thumbnail">
        <input type="file" name='gambar' class="form-control-file" >
      </div>

      @error('gambar')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror


    <div class="form-group">
    <label>Category</label>
   <select name="category_id_category" class="form-control">

        <option value="">--pilih category--</option>
        @forelse ($category as $item)
            @if ($item->id_category === $exhibition->category_id_category)
                <option value="{{$item->id_category}}" selected>{{$item->judul_category}} </option>
            @else
            <option value="{{$item->id_category}}">{{$item->judul_category}} </option> 
            @endif

        @empty
            <option value="">Belum ada data</option>
        @endforelse


   </select>
    </div>
  
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label >Deskripsi</label>
      <textarea class="form-control" name='deskripsi' cols='30' rows='10'>{{$exhibition->deskripsi}} </textarea>
    </div>
    
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    
   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection