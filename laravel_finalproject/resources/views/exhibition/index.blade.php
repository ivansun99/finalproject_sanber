@extends('layout.master')
@section('title')
    Halaman Exhibition
@endsection


@section('content')
<a href="/exhibition/create" class="btn btn-info mb-2">Add</a>

<div class="row">
    @forelse ($exhibition as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('/image/'. $item->gambar)}} " alt="Card image cap">
            <div class="card-body">
              <h2>{{$item->judul}} </h2>
              <p class="card-text">{{Str::limit($item->deskripsi,30)}} </p>
              <a href="/exhibition/{{$item->id_exhibition}} " class="btn btn-outline-primary btn-block mb-2">View</a>

              <form action="/exhibition/{{$item->id_exhibition}}" method="post">
                @csrf
                @method('delete')
                <a href="/exhibition/{{$item->id_exhibition}}/edit " class="btn btn-outline-warning btn-block">Edit</a>

                <input type="submit" value="Delete" class="btn btn-outline-danger  btn-block">
              </form>


            </div>
        </div>        
    </div>

    @empty
        <h1>Belum ada data</h1>
    @endforelse
  
</div>



@endsection