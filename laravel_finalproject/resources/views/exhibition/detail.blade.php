@extends('layout.master')
@section('title')
    Halaman Exhibition
@endsection


@section('content')
<div class="container-fluid">
    <div class="row">
            <img src="{{asset('/image/'. $exhibition->gambar)}} " alt="false" class="img-thumbnail">
        
        <div class="col-6">
            <h1>{{$exhibition->judul}} </h1>

            <h4>Category:</h4>
            <span class="badge badge-info p-2 ">{{$category->judul_category}} </span>
            <h4>Description: </h4>
            <p>{{$exhibition->deskripsi}} </p>
            <a href="/exhibition/" class="btn btn-secondary">Back to main</a>

        </div>
    </div>
</div>

@endsection