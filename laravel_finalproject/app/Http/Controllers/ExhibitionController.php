<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Exhibition;
use File;

class ExhibitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exhibition = Exhibition::all();
        return view('exhibition.index',['exhibition'=> $exhibition]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('exhibition.tambah',['category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'judul' => 'required',
            'gambar' => 'required|mimes:jpeg,jpg,png',
            'deskripsi' => 'required',
            'category_id_category' => 'required'
        ]); 

        //ubah nama file supaya unique
        $newNameImage = time().'.'.$request->gambar->extension();  
        //pindah file ke folder public
        $request->gambar->move(public_path('image'), $newNameImage);

        //masukkan data ke database   
        $exhibition = new Exhibition;
 
        $exhibition->judul = $request['judul'];
        $exhibition->gambar = $newNameImage;
        $exhibition->deskripsi = $request['deskripsi'];
        $exhibition->category_id_category = $request['category_id_category'];
        $exhibition->user_id_user = $request['category_id_category']; //ubah
 
        $exhibition->save();
        
        //return exhibition
        return redirect('/exhibition');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exhibition = Exhibition::where('id_exhibition',$id)->first();   
        $category= Category::where('id_category',$exhibition->category_id_category)->first(); 
        return view('exhibition.detail',['exhibition' => $exhibition, 'category'=> $category]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exhibition = Exhibition::where('id_exhibition',$id)->first();
        $category = Category::all();      
        return view('exhibition.edit',['exhibition' => $exhibition, 'category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_exhibition)
    {
        $validated = $request->validate([
            'judul' => 'required',
            'gambar' => 'mimes:jpeg,jpg,png',
            'deskripsi' => 'required',
            'category_id_category' => 'required'
        ]); 

        $exhibition = Exhibition::find($id_exhibition);
        //update
 
        $exhibition->judul = $request['judul'];
        $exhibition->deskripsi = $request['deskripsi'];
        $exhibition->category_id_category = $request['category_id_category'];
        $exhibition->user_id_user = $request['category_id_category'];

        if($request->has('gambar')){
            $path= 'image/';
            File::delete($path . $exhibition->gambar);
            $newNameImage = time().'.'.$request->gambar->extension();  
            //pindah file ke folder public
            $request->gambar->move(public_path('image'), $newNameImage);

            $exhibition->gambar=$newNameImage;
        }
        
        $exhibition->save();

        return redirect('/exhibition');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_exhibition)
    {
        $exhibition = Exhibition::find($id_exhibition);
        $path= 'image/';
        File::delete($path . $exhibition->gambar);
        $exhibition->delete();
        return redirect('/exhibition');
    }
}
