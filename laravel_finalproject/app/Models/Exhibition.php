<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exhibition extends Model
{
    use HasFactory;

    protected $table ='exhibition';
    protected $primaryKey = 'id_exhibition';
    protected $fillable = ['judul', 'gambar','deskripsi','user_id_user','category_id_category'];


}
