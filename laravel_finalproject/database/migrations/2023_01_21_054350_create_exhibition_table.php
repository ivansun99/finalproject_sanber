<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExhibitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibition', function (Blueprint $table) {
            $table->id('id_exhibition');
            $table->string('judul');
            $table->string('gambar');
            $table->text('deskripsi');
            $table->unsignedBigInteger('user_id_user');
 
            $table->foreign('user_id_user')->references('id_user')->on('users');

            $table->unsignedBigInteger('category_id_category');
 
            $table->foreign('category_id_category')->references('id_category')->on('category');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exhibition');
    }
}
